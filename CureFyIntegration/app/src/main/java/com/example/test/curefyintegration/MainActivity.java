package com.example.test.curefyintegration;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;

import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;
import java.net.URI;

import in.curefy.app.videocall.VideoCallActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    TextView textview;
    Button btnRegId;
    // EditText etRegId;
    GoogleCloudMessaging gcm;
    String regid;
    String PROJECT_NUMBER = "1";

    String PROJECT_NUMBER_TWO = "1";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        btnRegId = (Button) findViewById(R.id.btnGetRegId);
        //etRegId = (EditText) findViewById(R.id.etRegId);

        btnRegId.setOnClickListener(this);
        textview = (TextView) findViewById(R.id.textview);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

       /* if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("isNextWeek")) {
            boolean isNextWeek = getIntent().getExtras().getBoolean("isNextWeek");
        }*/
        if (getIntent().getExtras() != null) {
            Bundle bundle = getIntent().getBundleExtra(VideoCallActivity.INTENT_EXTRA_PATIENT_INFO);
            boolean isNextWeek = getIntent().getExtras().getBoolean("isNextWeek");
        }
        // SendIntentToCureFyActivity();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //here calling the VideoCallActivity
    private void SendIntentToCureFyActivity() {
        Intent intent = new Intent(this, VideoCallActivity.class);
        intent.putExtra(VideoCallActivity.INTENT_EXTRA_PATIENT_ID, "9999-1");
        intent.putExtra(VideoCallActivity.INTENT_EXTRA_Doctor_ID, "9999-1");
        intent.putExtra(VideoCallActivity.INTENT_EXTRA_ACTIVITY_TITLE, "Patient Screen");
        Bundle patientInfo = new Bundle();
        patientInfo.putString("Patient_Name", "Karthik");
        patientInfo.putInt("Age", 20);
        patientInfo.putString("Sex", "M");
        patientInfo.putString("Address", "Test Address");
        patientInfo.putString("Occupation", "Associates");
        patientInfo.putString("Patient_Summary", "Test Summary");
        intent.putExtra(VideoCallActivity.INTENT_EXTRA_PATIENT_INFO, patientInfo);
        startActivityForResult(intent, 5000);
    }

    // Call Back method  to get the Message form other Activity
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 5000) {
            // String message=data.getStringExtra("MESSAGE");
            String message = data.getStringExtra(VideoCallActivity.INTENT_EXTRA_DURATION);
            String patientUri = data.getStringExtra(VideoCallActivity.INTENT_EXTRA_PRESCRIPTION);
            Bundle bundle = data.getBundleExtra(VideoCallActivity.INTENT_EXTRA_PATIENT_INFO);
            String patient_name = bundle.getString("Patient_Name");
            textview.setText("Patient Details : "+patient_name+"__"+bundle.getInt("Age")+"__"+bundle.getString("Address")+"__"+bundle.getString("Occupation")+"__"+bundle.getString("Patient_Summary"));
        }
    }



   /* public void getRegId(){
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                    }
                    regid = gcm.register(PROJECT_NUMBER);
                    msg = "Device registered, registration ID=" + regid;
                    Log.i("GCM", msg);

                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();

                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                etRegId.setText(msg + "\n");
            }
        }.execute(null, null, null);
    }*/

    @Override
    public void onClick(View v) {
        SendIntentToCureFyActivity();
        //sendVideoCallNotification_Doctor        //getRegId();
               // ("Test", "Test_Sender", "9999-1");
        //getRegId();
    }


    private void sendVideoCallNotification_Doctor(String message,
                                           String sender, String s_id) {
        Intent i = new Intent(this, VideoCallActivity.class);
        i.putExtra(VideoCallActivity.INTENT_EXTRA_IS_NOTIFICATION, true);
        i.putExtra(VideoCallActivity.INTENT_EXTRA_VIDEO_SESSION_ID, s_id);
        i.putExtra(VideoCallActivity.INTENT_EXTRA_VIDEO_TOKEN, message);
        i.putExtra(VideoCallActivity.INTENT_EXTRA_GET_REQUESTEE, sender);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        int requestID = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getActivity(
                getApplicationContext(), requestID, i,
                PendingIntent.FLAG_UPDATE_CURRENT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(
                RingtoneManager.TYPE_RINGTONE);
        NotificationCompat.Builder notificationBuilder = new
                NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.curefy_icon)
                .setContentTitle("Doctor App")
                .setContentText(s_id)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setNumber(777)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(100,
                notificationBuilder.build());

    }

    private void doctorSideResultFromTheIntent() {

    }


}
