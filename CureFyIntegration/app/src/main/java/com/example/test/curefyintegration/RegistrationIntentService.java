package com.example.test.curefyintegration;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import in.curefy.app.videocall.Consts;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by satheesh on 12/1/2015.
 */
public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";

    private final String API_ENDPOINT = "http://server.curefy.in:8080/api/gcm/";

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        try {
// In the (unlikely) event that multiple refresh operations occur simultaneously,
            // ensure that they are processed sequentially.
            synchronized (TAG) {
// [START register_for_gcm]
                // Initially this call goes out to the network to retrieve the token, subsequent calls
                // are local.
                // [START get_token]
                InstanceID instanceID = InstanceID.getInstance(this);
                String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                        GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                // [END get_token]
                Log.i(TAG, "GCM Registration Token: " + token);

                sendRegistrationToServer(token);

                // You should store a boolean that indicates whether the generated token has been
                // sent to your server. If the boolean is false, send the token to your server,
                // otherwise your server should have already received the token.
                Vars.setGcmSentTokenToServer(true);
                // [END register_for_gcm]
            }
        } catch (Exception e) {
            Log.d(TAG, "Failed to complete token refresh", e);
        }
// Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(Consts.REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    /**
     * Persist registration to third-party servers.
     * <p/>
     * Modify this method to associate the user's GCM registration token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        Retrofit adapter = new Retrofit.Builder()
                .baseUrl(API_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        GcmRegistration api = adapter.create(GcmRegistration.class);

        GcmData data = new GcmData(token);
        Call<GcmRegResponse> call = api.registerGcmToken(data);
        call.enqueue(new Callback<GcmRegResponse>() {
            @Override
            public void onResponse(Response<GcmRegResponse> response, Retrofit retrofit) {
                Log.i(TAG, "GCM Response Status: "+ response.code());
            }

            @Override
            public void onFailure(Throwable t) {

                Log.i(TAG, "GCM Response Error: "+ t);
            }
        });
    }

    private interface GcmRegistration{
        @POST("register")
        Call<GcmRegResponse> registerGcmToken(@Body GcmData gcmData);
    }

    private class GcmData{
        String token;
        String gcm_token;

        public GcmData(String gcmToken){
            token = //set your doctor id here
                    this.gcm_token = gcmToken;
        }

    }

    private class GcmRegResponse{
        String status;
        public String getStatus(){
            return status;
        }
    }
}