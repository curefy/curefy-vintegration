package com.example.test.curefyintegration;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

import in.curefy.app.videocall.VideoCallActivity;

/**
 * Created by satheesh on 12/1/2015.
 */
public class GcmService extends GcmListenerService {

    private final String TAG = "GcmListenerService";
    static int numVideoMessagesNotification= 0;

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String action = data.getString("action");
        String message = data.getString("payload");
        String sender = data.getString("sender");
        String msg_id = data.getString("msg_id");

        startService(intent);
        Log.d(TAG, "from: " + from);
        switch(action){
            case "NEW_VIDEO_CALL_SESSION":
                sendVideoCallNotification(message, sender, msg_id);
                break;
        }
    }
    // [END receive_message]


    private void sendVideoCallNotification(String message, String sender, String session_id) {
        Intent i = new Intent(this, VideoCallActivity.class);
        i.putExtra(VideoCallActivity.INTENT_EXTRA_IS_NOTIFICATION, true);
        i.putExtra(VideoCallActivity.INTENT_EXTRA_VIDEO_SESSION_ID, session_id);
        i.putExtra(VideoCallActivity.INTENT_EXTRA_VIDEO_TOKEN, message);
        i.putExtra(VideoCallActivity.INTENT_EXTRA_GET_REQUESTEE, sender);
        int requestID = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),requestID,i,PendingIntent.FLAG_UPDATE_CURRENT);


        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.notification_template_icon_bg)
                .setContentTitle("New Video Call")
                .setContentText("Click to accept call")
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setNumber()
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify( /*ID of notification */, notificationBuilder.build());

    }

}
